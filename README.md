# Arek Motion API
Arek Motion (stands for Mobile Innovation Recruitment Application) is a recruitment platform for mobile laboratory at Telkom University. This repository is an example of using the API for recruitment purposes such as Member registration, login, and member selection. This API was made with **NodeJS, Express JS, and PostgreSQL**

---

## Tech stack
- Node JS (latest version)
- Express JS (v4.17.1)
- Express Promise Router (v4.1.0)
- Cors (v2.8.5)
- Dotenv (v8.2.0)
- Nodemon (v2.0.7)
- pg (v8.5.1)

## How To Run
After cloning this repository, you must run several commands

- Install the dependencies
    ```
    npm install
    ```
- Run the application in development mode
    ```
    npm run dev
    ```
